# Site Clearing Simulation

A construction simulator app built in React. Follow the steps below to play around. 

#### Desktop view

![Alt text](public/desktop.png?raw=true "Title")

#### Mobile view

![Alt text](public/mobile.png?raw=true "Title")

#### Link to the app hosted on GitLab 
 https://phanirocks.gitlab.io/ConstructionSimulator/ 

- Upload a text file containing the sitemap. 
- The map is a text file consisting of the characters 'o' (1 Fuel unit to visit), 't' (2 Fuel Units to visit),
'r' (2 Fuel units to visit) and 'T' (Unprotected tree which ends the simulation if visited) 
- The map consists of multiple lines in the form of a rectangle to represent the sitemap.
- Download the sample sitemap from the link to understand the format
- Upload the sitemap using the File Upload button the map
- Change direction and move the bulldozer using the control buttons
- The simulation ends if the end simulation button is clicked, the bulldozer hits an unprotected tree (marked as 'T')
or the bulldozer goes out of boundaries.
- While the bulldozer is moving, the commandList and itemized list are dynamically updated .
- When the simulation ends, a final report is generated indicating the total credits. All unvisited squares will account for
3 credits each. 

## To Run the application locally on your browser

1. From the project root directory, executing the following command 
 
   `npm start`

2. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.

## To Run unit tests

1. Run the app by executing the following command 
    
    `npm run test`

2. Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

3. To update snapshots , run 
   `npm run test -u`



