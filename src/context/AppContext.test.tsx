import React from "react";
import {render, screen, waitFor} from "@testing-library/react";
import {AppContext, AppContextProvider} from "./AppContext";
import {act} from "react-dom/test-utils";
import userEvent from "@testing-library/user-event";

const App =
    <AppContextProvider>
        <AppContext.Consumer>
            {value => (
                <>
                    <div>Current column is {value.currentColumn}</div>
                    <div>Current row is {value.currentRow}</div>
                    <div>Current direction is {value.currentDirection}</div>
                    <div>Current simulationStarted is {value.simulationStarted ? 'true' : 'false'}</div>
                    <div>Current simulationEnded is {value.simulationEnded ? 'true' : 'false'}</div>
                    <div>Command List length is {value.commandList.length}</div>
                    <div>Current Fuel Consumed {value.currentFuelConsumed}</div>
                    <button onClick={() => value.setSiteData([['o', 't'], ['o', 't']])}>Set Site Data</button>
                    <button onClick={() => value.advanceColumn(1)}>AppendColumn</button>
                    <button onClick={() => value.advanceRow(1)}>AppendRow</button>
                    <button onClick={() => value.reset()}>Reset</button>
                </>
            )}
        </AppContext.Consumer>
    </AppContextProvider>;

describe('AppContext', () => {
    it('Provides Context with default values for row, column, direction, fuel consumed, simulation started and simulation ended', () => {
        render(App);
        expect(screen.getByText('Current column is -1'));
        expect(screen.getByText('Current row is 0'));
        expect(screen.getByText('Current direction is RIGHT'));
        expect(screen.getByText('Current Fuel Consumed 0'));
        expect(screen.getByText('Current simulationStarted is false'));
        expect(screen.getByText('Current simulationEnded is false'));
    });

    describe('advanceColumn function in Context', () => {

        describe('When Site Data is loaded', () => {
            it('Advances column value by the amount passed', async () => {
                render(App);
                await waitFor(() => screen.getByText('Current column is -1'));
                await act(async () => {
                    userEvent.click(screen.getByText('Set Site Data'));
                    userEvent.click(screen.getByText('AppendColumn'));
                });
                await waitFor(() => screen.getByText('Current column is 0'));
                await waitFor(() => screen.getByText('Current simulationStarted is true'));
            });

            it('Ends the simulation when column count is greater than the length of columns in siteData', async () => {
                render(App);
                await act(async () => {
                    userEvent.click(screen.getByText('Set Site Data'));
                });
                await act(async () => {
                    userEvent.click(screen.getByText('AppendColumn'));
                });
                await act(async () => {
                    userEvent.click(screen.getByText('AppendColumn'));
                });
                await act(async () => {
                    userEvent.click(screen.getByText('AppendColumn'));
                });
                await waitFor(() => screen.getByText('Current column is -1'));
                await waitFor(() => screen.getByText('Current simulationEnded is true'));
            });

            it('Appends to CommandList and increments fuel consumed', async () => {
                render(App);
                expect(screen.getByText('Command List length is 0'));
                expect(screen.getByText('Current Fuel Consumed 0'));
                await act(async () => {
                    userEvent.click(screen.getByText('Set Site Data'));
                    userEvent.click(screen.getByText('AppendColumn'));
                });
                await waitFor(() => screen.getByText('Command List length is 1'));
                await waitFor(() => screen.getByText('Current Fuel Consumed 1'));
            });
        });

        describe('When Site Data is not loaded', () => {
            it('Does not advance column value by the amount passed', async () => {
                render(App);
                await waitFor(() => screen.getByText('Current column is -1'));
                await act(async () => {
                    userEvent.click(screen.getByText('AppendColumn'));
                });
                await waitFor(() => screen.getByText('Current column is -1'));
                await waitFor(() => screen.getByText('Current simulationStarted is false'));
            });
        });
    });

    describe('advanceRow function in Context', () => {

        it('advances row count by the amount passed when siteData is set', async () => {
            render(App);
            await waitFor(() => screen.getByText('Current row is 0'));
            await act(async () => {
                userEvent.click(screen.getByText('Set Site Data'));
            });
            await act(async () => {
                userEvent.click(screen.getByText('AppendColumn'));
            });
            await act(async () => {
                userEvent.click(screen.getByText('AppendRow'));
            });
            await waitFor(() => screen.getByText('Current row is 1'));

        });

        it('ends the simulation when row count after appending is higher than the number of rows in sitemap', async () => {
            render(App);
            await act(async () => {
                userEvent.click(screen.getByText('Set Site Data'));
            });
            await act(async () => {
                userEvent.click(screen.getByText('AppendColumn'));
            });
            await waitFor(() => screen.getByText('Current simulationEnded is false'));
            await act(async () => {
                userEvent.click(screen.getByText('AppendRow'));
            });
            await act(async () => {
                userEvent.click(screen.getByText('AppendRow'));
            });
            await waitFor(() => screen.getByText('Current simulationEnded is true'));
        });

        it('Appends to CommandList and increments fuel consumed', async () => {
            render(App);
            expect(screen.getByText('Command List length is 0'));
            expect(screen.getByText('Current Fuel Consumed 0'));
            await act(async () => {
                userEvent.click(screen.getByText('Set Site Data'));
            });
            await act(async () => {
                userEvent.click(screen.getByText('AppendColumn'));
            });
            await act(async () => {
                userEvent.click(screen.getByText('AppendRow'));
            });
            await waitFor(() => screen.getByText('Command List length is 2'));
            await waitFor(() => screen.getByText('Current Fuel Consumed 2'));
        });
    });

    describe('reset function in Context', () => {
        it('resets values to defaults', async () => {
            render(App);
            await act(async () => {
                userEvent.click(screen.getByText('Set Site Data'));
            });
            await act(async () => {
                userEvent.click(screen.getByText('AppendColumn'));
            });
            await act(async () => {
                userEvent.click(screen.getByText('AppendRow'));
            });

            await waitFor(() => screen.getByText('Current simulationStarted is true'));
            expect(screen.getByText('Current row is 1'));
            expect(screen.getByText('Current column is 0'));

            await act(async () => {
                userEvent.click(screen.getByText('Reset'));
            });

            await waitFor(() => screen.getByText('Current simulationStarted is false'));
            expect(screen.getByText('Current row is 0'));
            expect(screen.getByText('Current column is -1'));
        });
    });
});

