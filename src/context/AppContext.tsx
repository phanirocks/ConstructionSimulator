import React, {useState, ReactChild, ReactChildren, useEffect} from "react";

export type AppContextType = {
    siteData: Array<Array<String>>,
    setSiteData: Function,
    currentDirection: String,
    setCurrentDirection: Function,
    currentRow: number,
    advanceRow: Function,
    currentColumn: number,
    advanceColumn: Function,
    reset: Function,
    commandList: Array<String>,
    setCommandList: Function,
    itemizedCostList: Array<String>,
    setItemizedCostList: Function,
    currentFuelConsumed: number,
    setCurrentFuelConsumed: Function,
    unclearedFuel: number,
    setUnclearedFuel: Function,
    simulationStarted: boolean,
    setSimulationStarted: Function,
    simulationEnded: boolean,
    setSimulationEnded: Function
}

const initialValue: AppContextType = {
    siteData: [],
    setSiteData: () => {
    },
    currentDirection: 'RIGHT',
    setCurrentDirection: () => {
    },
    currentRow: 0,
    advanceRow: () => {
    },
    currentColumn: -1,
    advanceColumn: () => {
    },
    reset: () => {
    },
    commandList: [],
    setCommandList: () => {
    },
    itemizedCostList: [],
    setItemizedCostList: () => {
    },
    currentFuelConsumed: 0,
    setCurrentFuelConsumed: () => {
    },
    unclearedFuel: 0,
    setUnclearedFuel: () => {
    },
    simulationStarted: false,
    setSimulationStarted: () => {
    },
    simulationEnded: false,
    setSimulationEnded: () => {
    }
};


const AppContext = React.createContext<AppContextType>(initialValue);

interface ContextProviderProps {
    children: ReactChild | ReactChildren;
}

function AppContextProvider({children}: ContextProviderProps) {

    const [siteData, setSiteData] = useState<Array<Array<String>>>([]);
    const [currentDirection, setCurrentDirection] = useState('RIGHT');
    const [currentRow, setCurrentRow] = useState(0);
    const [currentColumn, setCurrentColumn] = useState(-1);
    const [commandList, setCommandList] = useState<Array<string>>([]);
    const [itemizedCostList, setItemizedCostList] = useState<Array<string>>([]);
    const [currentFuelConsumed, setCurrentFuelConsumed] = useState(0);
    const [unclearedFuel, setUnclearedFuel] = useState(0);
    const [unvisitedSet, setUnvisitedSet] = useState(new Set());
    const [simulationEnded, setSimulationEnded] = useState(false);
    const [simulationStarted, setSimulationStarted] = useState(false);

    useEffect(() => {
        if (currentColumn === -1) {
            if (simulationStarted) {
                setSimulationEnded(true);
                reset();
            }
        } else {
            updateSiteData();
        }
    }, [currentColumn, currentRow]);

    useEffect(() => {
        if (currentColumn != -1) {
            setCommandList([...commandList, `Change direction to ${currentDirection}`]);
        }
    }, [currentDirection]);

    const reset = () => {
        setSiteData([]);
        setCurrentDirection('RIGHT');
        setCurrentRow(0);
        setCurrentColumn(-1);
        setSimulationStarted(false);
        setUnvisitedSet(new Set());
    };

    const updateSiteData = () => {
        setCommandList([...commandList, `Advance`]);
        const siteDataCopy = siteData;
        if (currentRow < 0 || currentRow > siteData.length - 1 || currentColumn < 0 || currentColumn > siteData[0].length - 1) {
            setSimulationEnded(true);
            reset();
            return;
        }
        if (!unvisitedSet.has(`${currentRow}_${currentColumn}`)) {
            const unvisitedSetCopy = unvisitedSet;
            unvisitedSetCopy.add(`${currentRow}_${currentColumn}`);
            setUnvisitedSet(unvisitedSetCopy);
            setUnclearedFuel(unclearedFuel - 3);
        }
        const currentCell = siteDataCopy[currentRow][currentColumn];
        switch (currentCell) {
            case 'o':
                setCurrentFuelConsumed(currentFuelConsumed + 1);
                setItemizedCostList([...itemizedCostList, 'Plain Land: 1 FU']);
                break;
            case 't':
                siteDataCopy[currentRow][currentColumn] = 'o';
                setSiteData(siteDataCopy);
                setCurrentFuelConsumed(currentFuelConsumed + 2);
                setItemizedCostList([...itemizedCostList, 'Trees: 2 FU']);
                break;
            case 'r':
                siteDataCopy[currentRow][currentColumn] = 'o';
                setSiteData(siteDataCopy);
                setCurrentFuelConsumed(currentFuelConsumed + 2);
                setItemizedCostList([...itemizedCostList, 'Rocky: 2 FU']);
                break;
            case 'T':
                setSimulationEnded(true);
                reset();
                break;
            default:
                break;
        }
    };

    const advanceRow = (count: number) => {
        setCurrentRow(currentRow + count);
    };

    const advanceColumn = (count: number) => {
        if (!simulationStarted && currentColumn === -1) {
            setSimulationStarted(true);
        }
        setCurrentColumn(currentColumn + count);
    };


    return (
        <AppContext.Provider value={{
            siteData,
            setSiteData,
            currentDirection,
            setCurrentDirection,
            currentRow,
            advanceRow,
            currentColumn,
            advanceColumn,
            reset,
            commandList,
            setCommandList,
            itemizedCostList,
            setItemizedCostList,
            currentFuelConsumed,
            setCurrentFuelConsumed,
            unclearedFuel,
            setUnclearedFuel,
            simulationStarted,
            setSimulationStarted,
            simulationEnded,
            setSimulationEnded,
        }}>
            {children}
        </AppContext.Provider>
    );

}

export {AppContextProvider, AppContext};