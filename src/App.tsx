import React, {useContext} from 'react';
import './App.css';
import SiteMapUploader from "./components/SiteMapUploader/SiteMapUploader";
import {AppContext} from "./context/AppContext";
import {Grid} from "./components/Grid/Grid";
import {Controller} from "./components/Controller/Controller";
import CommandList from "./components/CommandList/CommandList";
import Bulldozer from "./components/Bulldozer/Bulldozer";
import ItemizedCostReport from "./components/ItemizedCostReport/ItemizedCostReport";
import FinalReport from "./components/FinalReport/FinalReport";
import Instructions from "./components/Instructions/Instructions";

function App() {
    const {siteData, simulationStarted, simulationEnded} = useContext(AppContext);
      return (
        <div className="App">
            <SiteMapUploader/>
            <Instructions/>
            <div className='controller-command-container'>
                <CommandList/>
                <ItemizedCostReport/>
                <Controller/>
             </div>
            <div className={'grid-container'}>
                <div className={'inner-grid'}>
                {siteData && !simulationStarted &&siteData.length>0 && <div className='bulldozer-container'><Bulldozer direction={'RIGHT'}/></div> }
                {siteData && <Grid/>}
                </div>
            </div>
            <div className={'finalreport-container'}>
                {simulationEnded && <FinalReport/>}
            </div>
        </div>
      );
}

export default App;
