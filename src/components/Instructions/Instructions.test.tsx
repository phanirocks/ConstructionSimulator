import React from "react";
import {render, screen} from "@testing-library/react";
import Instructions from "./Instructions";
import {act} from "react-dom/test-utils";
import userEvent from "@testing-library/user-event";

describe('Instructions', () => {
    it('matches the snapshot', () => {
        expect(render(<Instructions/>)).toMatchSnapshot();
    });
    it('by default hides the instructions', () => {
        render(<Instructions/>);
        expect(screen.queryByText('Upload the sitemap using the File Upload button')).toBeNull();
    });
    it('changes button text to Hide Instructions and displays instructions when show instructions is clicked', async () => {
        render(<Instructions/>);
        expect(screen.queryByText('Upload the sitemap using the File Upload button')).toBeNull();
        await act(async () => {
            userEvent.click(screen.getByText('Instructions'));
        });
        expect(screen.queryByText('Upload the sitemap using the File Upload button')).not.toBeNull();
        expect(screen.getByText('Hide Instructions'));
    });

    it('changes button text to Instructions and hides instructions when Instructions is clicked', async () => {
        render(<Instructions/>);
        await act(async () => {
            userEvent.click(screen.getByText('Instructions'));
        });
        expect(screen.queryByText('Upload the sitemap using the File Upload button')).not.toBeNull();
        expect(screen.getByText('Hide Instructions'));
        await act(async () => {
            userEvent.click(screen.getByText('Hide Instructions'));
        });
        expect(screen.queryByText('Upload the sitemap using the File Upload button')).toBeNull();
        expect(screen.getByText('Instructions'));
    });
});