import React, {useState} from "react";
import './Instructions.css';

export default function Instructions() {
    const [showInstructions, setShowInstructions] = useState(false);
    return <>
        <div className={'toggle-instructions'}>
            <button className={'toggle-button'} onClick={() => setShowInstructions(!showInstructions)}>
                {showInstructions ? 'Hide Instructions' : 'Instructions'}
            </button>
        </div>
        <div className={'instructions-container'}>
            {showInstructions && <ul>
                <li>The sitemap is a text file consisting of the characters `o` (1 Fuel unit to visit), `t` (2 Fuel Units to
                    visit),
                    `r` (2 Fuel units to visit) and `T` (Unprotected tree which ends the simulation if visited)
                </li>
                <li>The sitemap consists of multiple lines in the form of a rectangle to represent the site</li>
                <li>Download the sample sitemap using the `Download Sample SiteMap` link to understand the format</li>
                <li>Upload the sitemap using the File Upload button</li>
                <li>Change direction and move the bulldozer using the control buttons</li>
                <li>The simulation ends if the end simulation button is clicked, the bulldozer hits an unprotected tree
                    (marked as `T`)
                    or the bulldozer goes out of boundaries.
                </li>
                <li>While the bulldozer is moving, the commandList and itemized list are dynamically updated .</li>
                <li>When the simulation ends, a final report is generated indicating the total credits. All unvisited
                    squares will account for
                    3 credits each.
                </li>
            </ul>}
        </div>
    </>;
}