import React from "react";
import {render, screen} from "@testing-library/react";
import {AppContext} from "../../context/AppContext";
import {testContextObject} from "../../fixtures";
import FinalReport from "./FinalReport";

describe('FinalReport', () => {

    const FinalReportWithContext =
        <AppContext.Provider
            value={{...testContextObject, currentFuelConsumed: 10, unclearedFuel: 100}}>
            <FinalReport/>
        </AppContext.Provider>;

    it('Matches the snapshot', () => {
        expect(render(FinalReportWithContext)).toMatchSnapshot();
    });

    it('renders the credits for fuel consumed, uncleared squares and total credits', () => {
        render(FinalReportWithContext);
        expect(screen.getByText('--Simulation has ended. Final land clearing operation costs--'));
        expect(screen.getByText('Credits for Fuel: 10'));
        expect(screen.getByText('Credits for uncleared land: 100'));
        expect(screen.getByText('Final Credits: 110'));
    });
});