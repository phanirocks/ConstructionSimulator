import React, {useContext} from "react";
import {AppContext} from "../../context/AppContext";
import './FinalReport.css';

export default function FinalReport() {
    const {currentFuelConsumed, unclearedFuel} = useContext(AppContext);
    return (
        <div className={'final-report'}>
            <h2>--Simulation has ended. Final land clearing operation costs--</h2>
            <div>Credits for Fuel: {currentFuelConsumed}</div>
            <div>Credits for uncleared land: {unclearedFuel}</div>
            <h3> Final Credits: {currentFuelConsumed + unclearedFuel}</h3>
        </div>
    );
}