import React from "react";
import {act, render, screen} from "@testing-library/react";
import {Controller} from "./Controller";
import {AppContext, AppContextType} from "../../context/AppContext";
import {testContextObject} from "../../fixtures";
import userEvent from "@testing-library/user-event";

describe('Controller', () => {

    interface ControllerWithContextProps {
        contextObject: AppContextType;
    }

    const ControllerWithContext = (props: ControllerWithContextProps) => {
        return <AppContext.Provider
            value={props.contextObject}>
            <Controller/>
        </AppContext.Provider>;
    };

    it('matches the snapshot', () => {
        expect(render(<Controller/>)).toMatchSnapshot();
    });

    it('disables all buttons when simulationStarted is false in Context and siteData is empty', () => {
        render(<ControllerWithContext contextObject={{...testContextObject, siteData: [], simulationStarted: false}}/>);
        expect(screen.getByText('Left')).toBeDisabled();
        expect(screen.getByText('Right')).toBeDisabled();
        expect(screen.getByText('Advance')).toBeDisabled();
        expect(screen.getByText('End Simulation')).toBeDisabled();
    });


    it('enables only advance button when siteDataArray is not empty and simulationStarted is false', () => {
        render(<ControllerWithContext
            contextObject={{...testContextObject, simulationStarted: false, siteData: [['o', 't']]}}/>);
        expect(screen.getByText('Advance')).toBeEnabled();
        expect(screen.getByText('Left')).toBeDisabled();
        expect(screen.getByText('Right')).toBeDisabled();
        expect(screen.getByText('End Simulation')).toBeDisabled();
    });

    it('enables all buttons when siteDataArray is not empty and simulationStarted is true', () => {
        render(<ControllerWithContext
            contextObject={{...testContextObject, simulationStarted: true, siteData: [['o', 't']]}}/>);
        expect(screen.getByText('Left')).toBeEnabled();
        expect(screen.getByText('Right')).toBeEnabled();
        expect(screen.getByText('Advance')).toBeEnabled();
        expect(screen.getByText('End Simulation')).toBeEnabled();
    });

    it('calls setCurrentDirection with parameter LEFT when current direction is TOP and left button is clicked', async () => {
        const setCurrentDirectionMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            setCurrentDirection: setCurrentDirectionMock, currentDirection: 'TOP'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Left'));
        });
        expect(setCurrentDirectionMock).toHaveBeenCalledTimes(1);
        expect(setCurrentDirectionMock).toHaveBeenCalledWith('LEFT');
    });

    it('calls setCurrentDirection with parameter TOP when current direction is RIGHT and left button is clicked', async () => {
        const setCurrentDirectionMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            setCurrentDirection: setCurrentDirectionMock, currentDirection: 'RIGHT'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Left'));
        });
        expect(setCurrentDirectionMock).toHaveBeenCalledTimes(1);
        expect(setCurrentDirectionMock).toHaveBeenCalledWith('TOP');
    });

    it('calls setCurrentDirection with parameter RIGHT when current direction is BOTTOM and left button is clicked', async () => {
        const setCurrentDirectionMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            setCurrentDirection: setCurrentDirectionMock, currentDirection: 'BOTTOM'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Left'));
        });
        expect(setCurrentDirectionMock).toHaveBeenCalledTimes(1);
        expect(setCurrentDirectionMock).toHaveBeenCalledWith('RIGHT');
    });

    it('calls setCurrentDirection with parameter BOTTOM when current direction is LEFT and left button is clicked', async () => {
        const setCurrentDirectionMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            setCurrentDirection: setCurrentDirectionMock, currentDirection: 'LEFT'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Left'));
        });
        expect(setCurrentDirectionMock).toHaveBeenCalledTimes(1);
        expect(setCurrentDirectionMock).toHaveBeenCalledWith('BOTTOM');
    });

    it('calls setCurrentDirection with parameter RIGHT when current direction is TOP and right button is clicked', async () => {
        const setCurrentDirectionMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            setCurrentDirection: setCurrentDirectionMock, currentDirection: 'TOP'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Right'));
        });
        expect(setCurrentDirectionMock).toHaveBeenCalledTimes(1);
        expect(setCurrentDirectionMock).toHaveBeenCalledWith('RIGHT');
    });

    it('calls setCurrentDirection with parameter BOTTOM when current direction is RIGHT and right button is clicked', async () => {
        const setCurrentDirectionMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            setCurrentDirection: setCurrentDirectionMock, currentDirection: 'RIGHT'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Right'));
        });
        expect(setCurrentDirectionMock).toHaveBeenCalledTimes(1);
        expect(setCurrentDirectionMock).toHaveBeenCalledWith('BOTTOM');
    });

    it('calls setCurrentDirection with parameter LEFT when current direction is BOTTOM and right button is clicked', async () => {
        const setCurrentDirectionMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            setCurrentDirection: setCurrentDirectionMock, currentDirection: 'BOTTOM'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Right'));
        });
        expect(setCurrentDirectionMock).toHaveBeenCalledTimes(1);
        expect(setCurrentDirectionMock).toHaveBeenCalledWith('LEFT');
    });

    it('calls setCurrentDirection with parameter TOP when current direction is LEFT and right button is clicked', async () => {
        const setCurrentDirectionMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            setCurrentDirection: setCurrentDirectionMock, currentDirection: 'LEFT'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Right'));
        });
        expect(setCurrentDirectionMock).toHaveBeenCalledTimes(1);
        expect(setCurrentDirectionMock).toHaveBeenCalledWith('TOP');
    });

    it('increments row count by 1 when current direction is BOTTOM and Advance is clicked', async () => {
        const advanceRowMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            advanceRow: advanceRowMock, currentDirection: 'BOTTOM'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Advance'));
        });
        expect(advanceRowMock).toHaveBeenCalledTimes(1);
        expect(advanceRowMock).toHaveBeenCalledWith(1);
    });

    it('increments column count by 1 when current direction is RIGHT and Advance is clicked', async () => {
        const advanceColumnMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            advanceColumn: advanceColumnMock, currentDirection: 'RIGHT'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Advance'));
        });
        expect(advanceColumnMock).toHaveBeenCalledTimes(1);
        expect(advanceColumnMock).toHaveBeenCalledWith(1);
    });

    it('decrements row count by 1 when current direction is TOP and Advance is clicked', async () => {
        const advanceRowMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            advanceRow: advanceRowMock, currentDirection: 'TOP'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Advance'));
        });
        expect(advanceRowMock).toHaveBeenCalledTimes(1);
        expect(advanceRowMock).toHaveBeenCalledWith(-1);
    });

    it('decrements column count by 1 when current direction is LEFT and Advance is clicked', async () => {
        const advanceColumnMock = jest.fn();
        render(<ControllerWithContext contextObject={{
            ...testContextObject,
            advanceColumn: advanceColumnMock, currentDirection: 'LEFT'
        }}/>);
        await act(async () => {
            userEvent.click(screen.getByText('Advance'));
        });
        expect(advanceColumnMock).toHaveBeenCalledTimes(1);
        expect(advanceColumnMock).toHaveBeenCalledWith(-1);
    });
});