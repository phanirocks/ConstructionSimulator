import React, {useContext} from "react";
import {AppContext} from "../../context/AppContext";
import './Controller.css';

export function Controller() {
    const {
        currentDirection,
        setCurrentDirection,
        advanceRow,
        advanceColumn,
        simulationStarted,
        simulationEnded,
        setSimulationEnded,
        siteData,
        reset
    } = useContext(AppContext);

    const handleLeftButtonClick = () => {
        switch (currentDirection) {
            case 'RIGHT':
                setCurrentDirection('TOP');
                break;
            case 'TOP':
                setCurrentDirection('LEFT');
                break;
            case 'LEFT':
                setCurrentDirection('BOTTOM');
                break;
            case 'BOTTOM':
                setCurrentDirection('RIGHT');
                break;
        }
    };

    const handleRightButtonClick = () => {
        switch (currentDirection) {
            case 'RIGHT':
                setCurrentDirection('BOTTOM');
                break;
            case 'BOTTOM':
                setCurrentDirection('LEFT');
                break;
            case 'LEFT':
                setCurrentDirection('TOP');
                break;
            case 'TOP':
                setCurrentDirection('RIGHT');
                break;
        }
    };

    const handleAdvance = () => {
        switch (currentDirection) {
            case 'LEFT':
                advanceColumn(-1);
                break;
            case 'RIGHT':
                advanceColumn(1);
                break;
            case 'TOP':
                advanceRow(-1);
                break;
            case 'BOTTOM':
                advanceRow(1);
                break;
        }
    };

    const handleEndSimulation = () => {
        setSimulationEnded(true);
        reset();
    };

    return (
        <div className={'controller-container'}>
            <h3> Controller </h3>
            <button className={'controller-button'} onClick={handleLeftButtonClick}
                    disabled={!simulationStarted || simulationEnded}>Left
            </button>
            <button className={'controller-button'} onClick={handleRightButtonClick}
                    disabled={!simulationStarted || simulationEnded}>Right
            </button>
            <button className={'controller-button'} onClick={handleAdvance}
                    disabled={siteData.length == 0 || simulationEnded}>Advance
            </button>
            <button className={'controller-button'} onClick={handleEndSimulation}
                    disabled={!simulationStarted || simulationEnded}>End Simulation
            </button>
        </div>);
}