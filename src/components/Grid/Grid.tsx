import React, {useContext} from "react";
import './Grid.css';
import {AppContext} from "../../context/AppContext";
import Bulldozer from "../Bulldozer/Bulldozer";

export function Grid() {
    const Context = useContext(AppContext);
    const {currentRow, currentColumn, currentDirection, siteData} = Context;
    const createAndPopulateCells = () => {
        const numberOfRows = siteData.length;
        const numberOfColumns = siteData[0]?.length;
        const cells = [];
        for (let i = 0; i < numberOfRows; i++) {
            for (let j = 0; j < numberOfColumns; j++) {
                const isActive = i === currentRow && j === currentColumn;
                cells.push(<div key={`${i}+${j}`} id={`${i}+${j}`} className='cell' data-testid='cell'>
                    {!isActive && siteData[i][j]}
                    {isActive && <Bulldozer direction={currentDirection.toString()}/>}
                </div>);
            }
        }
        return cells;
    };
    return (
        <div className='grid' style={{gridTemplateColumns: `repeat(${siteData[0] ? siteData[0].length : 0}, 1fr)`}}>
            {createAndPopulateCells()}
        </div>);
}