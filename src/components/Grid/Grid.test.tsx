import React from "react";
import {render, screen, waitFor} from "@testing-library/react";
import {Grid} from "./Grid";
import {AppContext} from "../../context/AppContext";
import {testContextObject} from "../../fixtures";


describe('Grid', () => {

    const GridWithContext =
        <AppContext.Provider value={{...testContextObject, siteData: [['o', 'T', 't'], ['o', 'T', 't']]}}>
            <Grid/>
        </AppContext.Provider>;

    it('matches the snapshot', () => {
        expect(render(GridWithContext)).toMatchSnapshot();
    });

    it('creates a grid with of size n*m when the context contains siteData array of size n*m', async () => {
        render(GridWithContext);
        expect(screen.getAllByTestId('cell').length).toEqual(6);
        expect(screen.getAllByText('o').length).toEqual(1);
        expect(screen.getAllByText('t').length).toEqual(2);
        expect(screen.getAllByText('T').length).toEqual(2);
    });
});