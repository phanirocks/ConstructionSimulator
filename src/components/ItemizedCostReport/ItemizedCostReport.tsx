import React, {useContext} from "react";
import {AppContext} from "../../context/AppContext";
import './ItemizedCostReport.css';

export default function ItemizedCostReport() {
    const {itemizedCostList, currentFuelConsumed} = useContext(AppContext);
    const renderItemizedCostList = () => {
        return itemizedCostList.map((item, index) => <div key={index} className={'item'}>{item}</div>);
    };
    return <div className={'itemized-container'}>
                <h3>Itemized Expense Report</h3>
                <div className={'itemized-list'}>
                     {renderItemizedCostList()}
                </div>
                <p><strong>Total Fuel Units Consumed:</strong> {currentFuelConsumed} </p>
        </div>;
}