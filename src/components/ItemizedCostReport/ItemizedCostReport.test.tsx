import React from "react";
import {render, screen} from "@testing-library/react";
import ItemizedCostReport from "./ItemizedCostReport";
import {AppContext} from "../../context/AppContext";
import {testContextObject} from "../../fixtures";

describe('ItemizedCostReport', () => {

    const ItemizedCostReportWithContext =
        <AppContext.Provider value={{...testContextObject, itemizedCostList: ['Tree 1 FU', 'Plain 1 FU']}}>
            <ItemizedCostReport/>
        </AppContext.Provider>;

    it('matches the snapshot', () => {
        expect(render(ItemizedCostReportWithContext)).toMatchSnapshot();
    });

    it('displays the itemized report in the context', () => {
        render(ItemizedCostReportWithContext);
        expect(screen.getByText('Tree 1 FU'));
        expect(screen.getByText('Plain 1 FU'));
    });
});