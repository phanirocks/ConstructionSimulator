import {render, fireEvent, screen, act, waitFor} from "@testing-library/react";
import React from "react";
import SiteMapUploader from "./SiteMapUploader";

describe('Header', () => {

    it('matches the snapshot', () => {
        expect(render(<SiteMapUploader/>)).toMatchSnapshot();
    });

    describe('When a file with valid characters is uploaded', () => {
        it('renders a message stating that the simulation started', async () => {
            render(<SiteMapUploader/>);
            const file = new File(['oTt'], 'validdata.txt', {type: 'text/plain'});
            const fileInput = screen.getByAltText('File Upload');
            await act(async () => {
                fireEvent.change(fileInput, {target: {files: [file]}});
            });
            await waitFor(() => screen.getByText('Simulation Started'));
        });
    });

    describe('When a file with invalid characters is uploaded', () => {
        it('renders error stating that file is invalid on the screen', async () => {
            render(<SiteMapUploader/>);
            const file = new File(['abcd'], 'invaliddata.txt', {type: 'txt'});
            const fileInput = screen.getByAltText('File Upload');
            await act(async () => {
                fireEvent.change(fileInput, {target: {files: [file]}});
            });
            await waitFor(() => screen.getByText('Invalid File Format'));
        });
    });

    describe('When a file with invalid type is uploaded', () => {
        it('renders error stating that file is invalid on the screen', async () => {
            render(<SiteMapUploader/>);
            const file = new File(['abcd'], 'invaliddata.txt', {type: 'img'});
            const fileInput = screen.getByAltText('File Upload');
            await act(async () => {
                fireEvent.change(fileInput, {target: {files: [file]}});
            });
            await waitFor(() => screen.getByText('Invalid File Format'));
        });
    });
});