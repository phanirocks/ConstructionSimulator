import React, {useState, useContext} from "react";
import {AppContext} from "../../context/AppContext";
import './SiteMapUploader.css';

export default function SiteMapUploader() {
    const {setSiteData, reset, setUnclearedFuel, simulationEnded, setSimulationEnded, setCurrentFuelConsumed, setCommandList, setItemizedCostList} = useContext(AppContext);
    const [message, setMessage] = useState('Upload a sitemap file to begin simulation');
    const validSymbols = new Set(['o', 't', 'r', 'T']);

    const handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        setSiteData([]);
        setCommandList([]);
        setItemizedCostList([]);
        setUnclearedFuel(0);
        setCurrentFuelConsumed(0);
        reset();
        const files = e.target.files;
        if (files !== null) {
            if (files[0] !== null) {
                if (!files[0].type.match('text/plain')) {
                    setMessage('Invalid File Format');
                    return;
                }
                const reader = new FileReader();
                reader.readAsText(files[0]);
                reader.onload = async () => {
                    const result = reader.result?.toString() || '';
                    const rows = result?.split('\n');
                    const siteMap = [];
                    let unclearedFuel = 0;
                    for (let i = 0; i < rows.length ; i++) {
                        const rowData = rows[i].split('');
                        //To handle the case when last row contains only /n
                        if(i===rows.length-1&&rowData.length==0){
                            break;
                        }
                        const siteMapRow = [];
                        for (let j = 0; j < rowData.length; j++) {
                            if (!validSymbols.has(rowData[j])) {
                                setMessage('Invalid File format');
                                break;
                            }
                            siteMapRow.push(rowData[j]);
                            unclearedFuel += 3;
                        }
                        siteMap.push(siteMapRow);
                    }
                    setMessage('Simulation Started');
                    setSimulationEnded(false);
                    setSiteData(siteMap);
                    setUnclearedFuel(unclearedFuel);
                };
            } else {
                setMessage('Some Unexpected Error occured');
            }
        } else {
            setMessage('Some Unexpected Error occured');
        }
    };
    return (
        <div className={'uploader-container'}>
            <div className={'message'}>
                <strong>
                {!simulationEnded && message}
                {simulationEnded && 'Upload a sitemap file to begin simulation'}
                </strong>
            </div>
            <div className={'file-input-container'}>
                <input className={'file-input'} type="file" name="file" key={Date.now()} onChange={handleChange}
                       alt='File Upload'/>
                <a download href='./samplemap.txt'>Download Sample SiteMap</a>
            </div>
        </div>);
}