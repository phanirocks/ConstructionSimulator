import React, {useContext} from "react";
import {AppContext} from "../../context/AppContext";
import './CommandList.css';

export default function CommandList() {
    const {commandList} = useContext(AppContext);
    const renderCommands = (commandList: Array<String>) => {
        return commandList.map((command: String, index: number) =>
            <div key={index} className={'command-box'}>{command}</div>);
    };
    return (
        <div className='commandlist-container'>
            <h3> Command List </h3>
            <div className={'commands-list'}>
                {renderCommands(commandList)}
            </div>
        </div>);
}