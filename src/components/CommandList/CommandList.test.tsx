import React from "react";
import {render, screen} from "@testing-library/react";
import CommandList from "./CommandList";
import {AppContext} from "../../context/AppContext";
import {testContextObject} from "../../fixtures";

describe('CommandList', () => {

    const CommandListWithContext =
        <AppContext.Provider
            value={{...testContextObject, commandList: ['foo', 'bar']}}>
            <CommandList/>
        </AppContext.Provider>;

    it('Matches the snapshot', () => {
        expect(render(CommandListWithContext)).toMatchSnapshot();
    });

    it('Renders the current commands available in Context', () => {
        render(CommandListWithContext);
        expect(screen.getByText('foo'));
        expect(screen.getByText('bar'));
    });
});