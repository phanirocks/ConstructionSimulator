import React from "react";
import {render} from "@testing-library/react";
import Bulldozer from "./Bulldozer";

describe('Bulldozer', () => {
    it('matches snapshot when direction is LEFT', () => {
        expect(render(<Bulldozer direction={'LEFT'}/>)).toMatchSnapshot();
    });
    it('matches snapshot when direction is RIGHT', () => {
        expect(render(<Bulldozer direction={'RIGHT'}/>)).toMatchSnapshot();
    });
    it('matches snapshot when direction is TOP', () => {
        expect(render(<Bulldozer direction={'TOP'}/>)).toMatchSnapshot();
    });
    it('matches snapshot when direction is BOTTOm', () => {
        expect(render(<Bulldozer direction={'BOTTOM'}/>)).toMatchSnapshot();
    });
});