import React from "react";
import './Bulldozer.css';

interface BulldozerProps {
    direction: string;
}

export default function Bulldozer(props: BulldozerProps) {
    const getClassName = () => {
        switch (props.direction) {
            case 'LEFT':
                return 'left';
            case 'RIGHT':
                return 'right';
            case 'TOP':
                return 'top';
            case 'BOTTOM':
                return 'bottom';
            default:
                return 'left';
        }
    };
    return <i className={`bulldozer ${getClassName()}`}/>;
}