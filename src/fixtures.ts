
export const testContextObject = {
    siteData: [['o','t'],['r','T']],
    setSiteData: ()=>{},
    currentDirection: 'RIGHT',
    setCurrentDirection: ()=>{},
    currentRow: 0,
    advanceRow: ()=>{},
    currentColumn: 0,
    advanceColumn: ()=>{},
    reset: ()=>{},
    commandList: [],
    itemizedCostList: [],
    currentFuelConsumed: 0,
    setCurrentFuelConsumed: ()=>{},
    unclearedFuel: 0,
    setUnclearedFuel: ()=>{},
    simulationStarted: true,
    setSimulationStarted: ()=> {},
    simulationEnded: false,
    setSimulationEnded: ()=> {}
};